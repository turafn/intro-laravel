<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="post">
        @csrf
        <p>First name:</p> 
        <p><input type="text" id="fname" name="fname"></p>

        <p>Last name:</p>
        <p><input type="text" id="lname" name="lname"></p>

        <p>Gender:</p>
        <p><input type="radio"> Male </p>
        <p><input type="radio"> Female </p>
        <p><input type="radio"> Other </p>

        <p> Nationality:</p>
        <select>
            <option>Indonesian</option>
            <option>Inggris</option>
            <option>Jerman</option>
        </select>

        <p> Language Spoken: </p>
        <p> <input type="checkbox"> Bahasa Indonesia </p>
        <p> <input type="checkbox"> English <br> </p>
        <p> <input type="checkbox"> Other <br> </p>
        <p>Bio :</p>
        <textarea cols="30" rows="10"></textarea>
        <p>
            <form action="welcome" method="post">
                <button type="submit">Sign Up</button>
            </form>
        </p>
    </form>
</body>

</html>