<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function show(Request $request)
    {
        $fname = $request['fname'];
        $lname = $request['lname'];
        return view('welcome', compact('fname','lname'));
    }
    
    public function register()
    {
        return view('register');
    }
}
